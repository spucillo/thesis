import numpy as np


class DenseLayer:
    def __init__(self, input_size, output_size):
        self.input_size = input_size
        self.output_size = output_size
        # Initialize weights and biases with small random values
        self.weights = np.random.randn(input_size, output_size)
        self.biases = np.zeros(output_size)

    def forward(self, input_data):
        # Perform the forward pass of the layer
        self.input = input_data
        self.output = np.dot(input_data, self.weights) + self.biases

    def backward(self, d_output):
        # Perform the backward pass to update gradients
        self.d_weights = np.dot(self.input.T, d_output)
        self.d_biases = np.sum(d_output, axis=0)
        self.d_input = np.dot(d_output, self.weights.T)

    def update(self, learning_rate):
        # Update the weights and biases using gradient descent
        self.weights -= learning_rate * self.d_weights
        self.biases -= learning_rate * self.d_biases


def spmv(data, indices, indptr, vector):
    result = np.zeros((indptr.shape[0] - 1, vector.shape[1]))
    for i in range(indptr.shape[0] - 1):
        start_idx = indptr[i]
        end_idx = indptr[i + 1]
        result[i] = np.sum(
            data[start_idx:end_idx] * vector[indices[start_idx:end_idx]], axis=0
        )

    return result


def spmm(data, indices, indptr, matrix):
    result = np.zeros((indptr.shape[0] - 1, matrix.shape[1]))

    for col in range(matrix.shape[1]):
        col_vector = matrix[:, col].reshape(-1, 1)
        result[:, col] = spmv(data, indices, indptr, col_vector).flatten()

    return result


class GraphConvolutionLayer:
    def __init__(self, input_size, output_size):
        self.input_size = input_size
        self.output_size = output_size
        self.weights = np.random.randn(input_size, output_size)
        self.biases = np.zeros(output_size)

    def forward(self, adjacency_matrix, features):
        self.input = features
        self.adjacency_matrix = adjacency_matrix
        self.support = np.dot(features, self.weights)
        self.output = (
            np.dot(adjacency_matrix, self.support) + self.biases
        )  # TODO change to sparse

    def backward(self, d_output):
        d_support = np.dot(self.adjacency_matrix.T, d_output)  # TODO change to sparse
        self.d_weights = np.dot(self.input.T, d_support)
        self.d_biases = np.sum(d_output, axis=0)
        self.d_input = np.dot(d_support, self.weights.T)

    def update(self, learning_rate):
        self.weights -= learning_rate * self.d_weights
        self.biases -= learning_rate * self.d_biases


# Define the activation function (e.g., sigmoid)
def sigmoid(x):
    return 1 / (1 + np.exp(-x))


# Create the input data
input_data = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])

# Create the labels for a simple OR gate
labels = np.array([[0], [1], [1], [1]])


# Define the neural network
class NeuralNetwork:
    def __init__(self):
        # Define the layers
        self.input_size = 2
        self.hidden_size = 3
        self.output_size = 1

        self.hidden_layer = DenseLayer(self.input_size, self.hidden_size)
        self.output_layer = DenseLayer(self.hidden_size, self.output_size)

    def forward(self, input_data):
        self.hidden_layer.forward(input_data)
        self.hidden_layer_output = sigmoid(self.hidden_layer.output)
        self.output_layer.forward(self.hidden_layer_output)

    def backward(self, labels):
        # Calculate the loss
        loss = 0.5 * np.sum((self.output_layer.output - labels) ** 2)

        # Backpropagate the error
        d_output = self.output_layer.output - labels
        self.output_layer.backward(d_output)
        self.hidden_layer.backward(self.output_layer.d_input)

        return loss

    def update(self, learning_rate):
        # Update the weights and biases of both layers
        self.output_layer.update(learning_rate)
        self.hidden_layer.update(learning_rate)


# Create the neural network
neural_net = NeuralNetwork()

# Training loop
learning_rate = 0.1
epochs = 10000

for epoch in range(epochs):
    neural_net.forward(input_data)
    loss = neural_net.backward(labels)
    neural_net.update(learning_rate)

    if epoch % 1000 == 0:
        print(f"Epoch {epoch}, Loss: {loss:.4f}")

# Test the trained neural network
neural_net.forward(input_data)
predictions = neural_net.output_layer.output

print("Final Predictions:")
print(predictions)
