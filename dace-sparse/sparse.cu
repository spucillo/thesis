/* DaCe AUTO-GENERATED FILE. DO NOT MODIFY */
#include <dace/dace.h>
#include "../../include/hash.h"

struct spmv_t {
    dace::cuda::Context *gpu_context;
    double * __restrict__ __0_gpu_x;
    double * __restrict__ __0_gpu_A_data;
    unsigned int * __restrict__ __0_gpu_A_indptr;
    unsigned int * __restrict__ __0_gpu_A_indices;
    double * __restrict__ __0_gpu_b;
};

DACE_EXPORTED void __dace_runkernel_assign_28_4_map_0_1_8(spmv_t *__state, double * __restrict__ gpu_b, int N);
DACE_EXPORTED void __dace_runkernel_single_state_body_map_0_0_6(spmv_t *__state, const double * __restrict__ gpu_A_data, const unsigned int * __restrict__ gpu_A_indices, const unsigned int * __restrict__ gpu_A_indptr, double * __restrict__ gpu_b, const double * __restrict__ gpu_x, int M, int N, int nnz);
void __program_spmv_internal(spmv_t *__state, double * __restrict__ A_data, unsigned int * __restrict__ A_indices, unsigned int * __restrict__ A_indptr, double * __restrict__ b, double * __restrict__ x, int M, int N, int nnz)
{

    {

        cudaMemcpyAsync(__state->__0_gpu_A_data, A_data, nnz * sizeof(double), cudaMemcpyHostToDevice, __state->gpu_context->streams[1]);
        cudaMemcpyAsync(__state->__0_gpu_A_indices, A_indices, nnz * sizeof(unsigned int), cudaMemcpyHostToDevice, __state->gpu_context->streams[2]);
        cudaMemcpyAsync(__state->__0_gpu_x, x, M * sizeof(double), cudaMemcpyHostToDevice, __state->gpu_context->streams[3]);
        cudaMemcpyAsync(__state->__0_gpu_A_indptr, A_indptr, (M + 1) * sizeof(unsigned int), cudaMemcpyHostToDevice, __state->gpu_context->streams[4]);
        __dace_runkernel_assign_28_4_map_0_1_8(__state, __state->__0_gpu_b, N);
        cudaStreamSynchronize(__state->gpu_context->streams[0]);
        cudaStreamSynchronize(__state->gpu_context->streams[1]);
        cudaStreamSynchronize(__state->gpu_context->streams[2]);
        cudaStreamSynchronize(__state->gpu_context->streams[3]);
        cudaStreamSynchronize(__state->gpu_context->streams[4]);


    }
    {

        __dace_runkernel_single_state_body_map_0_0_6(__state, __state->__0_gpu_A_data, __state->__0_gpu_A_indices, __state->__0_gpu_A_indptr, __state->__0_gpu_b, __state->__0_gpu_x, M, N, nnz);
        cudaMemcpyAsync(b, __state->__0_gpu_b, N * sizeof(double), cudaMemcpyDeviceToHost, __state->gpu_context->streams[0]);
        cudaStreamSynchronize(__state->gpu_context->streams[0]);


    }
}

DACE_EXPORTED void __program_spmv(spmv_t *__state, double * __restrict__ A_data, unsigned int * __restrict__ A_indices, unsigned int * __restrict__ A_indptr, double * __restrict__ b, double * __restrict__ x, int M, int N, int nnz)
{
    __program_spmv_internal(__state, A_data, A_indices, A_indptr, b, x, M, N, nnz);
}
DACE_EXPORTED int __dace_init_cuda(spmv_t *__state, int M, int N, int nnz);
DACE_EXPORTED int __dace_exit_cuda(spmv_t *__state);

DACE_EXPORTED spmv_t *__dace_init_spmv(int M, int N, int nnz)
{
    int __result = 0;
    spmv_t *__state = new spmv_t;


    __result |= __dace_init_cuda(__state, M, N, nnz);
    cudaMalloc((void**)&__state->__0_gpu_x, M * sizeof(double));
    cudaMalloc((void**)&__state->__0_gpu_A_data, nnz * sizeof(double));
    cudaMalloc((void**)&__state->__0_gpu_A_indptr, (M + 1) * sizeof(unsigned int));
    cudaMalloc((void**)&__state->__0_gpu_A_indices, nnz * sizeof(unsigned int));
    cudaMalloc((void**)&__state->__0_gpu_b, N * sizeof(double));

    if (__result) {
        delete __state;
        return nullptr;
    }
    return __state;
}

DACE_EXPORTED void __dace_exit_spmv(spmv_t *__state)
{
    cudaFree(__state->__0_gpu_x);
    cudaFree(__state->__0_gpu_A_data);
    cudaFree(__state->__0_gpu_A_indptr);
    cudaFree(__state->__0_gpu_A_indices);
    cudaFree(__state->__0_gpu_b);
    __dace_exit_cuda(__state);
    delete __state;
}

