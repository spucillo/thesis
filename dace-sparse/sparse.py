import copy
import dace
from dace import dtypes
from dace.transformation.auto import auto_optimize
from IPython.display import Code


M = dace.symbol("M", dtype=dace.int32)
N = dace.symbol("N", dtype=dace.int32)
nnz = dace.symbol("nnz", dtype=dace.int32)

# A has shape (M, N)
# x, y have shape (N, 1)


# Sparse matrix multiplication A * x = b
@dace.program
def spmv(
    A_data: dace.float64[nnz],
    A_indices: dace.uint32[nnz],
    A_indptr: dace.uint32[M + 1],
    x: dace.float64[M],
    b: dace.float64[N],
):
    b[:] = 0.0
    for i in range(M + 1):
        start = A_indptr[i]
        end = A_indptr[i + 1]
        cols = A_indices[start:end]
        vals = A_data[start:end]
        b[i] = dace.reduce(lambda a, b: a + b, vals * x[cols])


# From DaCe Paper
H = dace.symbol("H", dtype=dace.int32)
W = dace.symbol("W", dtype=dace.int32)

@dace.program
def spmv2(A_row: dace.uint32[H+1], A_col: dace.uint32[nnz],
            A_val: dace.float32 [nnz], x: dace.float32 [W],
            b: dace.float32[H]):
    for i in dace.map[0:H]:
        for j in dace.map[A_row[i]:A_row[i+1]]:
            with dace.tasklet :
                a << A_val[j]
                in_x << x[A_col[j]]
                out >> b(1, dace.sum)[i]
                out = a * in_x


# Generate SDFG
sdfg = spmv.to_sdfg()
cpu_sdfg = copy.deepcopy(sdfg)
gpu_sdfg = copy.deepcopy(sdfg)

# CPU code
cpu_sdfg = auto_optimize.auto_optimize(cpu_sdfg, device=dtypes.DeviceType.CPU)
code = Code(cpu_sdfg.generate_code()[0].clean_code, language="cpp")
with open("sparse.cpp", "w") as f:
    f.write(str(code))

# GPU code
gpu_sdfg = auto_optimize.auto_optimize(gpu_sdfg, device=dtypes.DeviceType.GPU)
code = Code(gpu_sdfg.generate_code()[0].clean_code, language="cuda")
with open("sparse.cu", "w") as f:
    f.write(str(code))
