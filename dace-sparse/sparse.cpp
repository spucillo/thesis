/* DaCe AUTO-GENERATED FILE. DO NOT MODIFY */
#include <dace/dace.h>
#include "../../include/hash.h"

struct spmv_t {

};

inline void reduce_1_1_11(spmv_t *__state, double* __restrict__ _in, double* __restrict__ _out, int end, int start) {

    {

        {
            for (auto _i0 = 0; _i0 < (end - start); _i0 += 1) {
                {
                    double __inp = _in[_i0];
                    double __out;

                    ///////////////////
                    // Tasklet code (identity)
                    __out = __inp;
                    ///////////////////

                    dace::wcr_fixed<dace::ReductionType::Sum, double>::reduce(_out, __out);
                }
            }
        }

    }
    
}

inline void loop_body_0_1_0(spmv_t *__state, double* __restrict__ A_data, unsigned int* __restrict__ A_indices, unsigned int* __restrict__ A_indptr, double* __restrict__ x, double* __restrict__ b, int M, int N, long long i, int nnz) {
    unsigned int start;
    unsigned int end;


    start = A_indptr[i];
    end = A_indptr[(i + 1)];
    {
        double __tmp4;
        double *__tmp3;
        __tmp3 = new double DACE_ALIGN(64)[(end - start)];

        {
            for (auto __i0 = 0; __i0 < (end - start); __i0 += 1) {
                double __s1_n4__out_n3IN_1;
                {
                    double* __arr = &x[0];
                    unsigned int __inp0 = A_indices[(__i0 + start)];
                    double __out;

                    ///////////////////
                    // Tasklet code (indirection)
                    __out = __arr[__inp0];
                    ///////////////////

                    __s1_n4__out_n3IN_1 = __out;
                }
                {
                    const double __in2 = __s1_n4__out_n3IN_1;
                    double __in1 = A_data[(__i0 + start)];
                    double __out;

                    ///////////////////
                    // Tasklet code (_Mult_)
                    __out = (__in1 * __in2);
                    ///////////////////

                    __tmp3[__i0] = __out;
                }
            }
        }
        reduce_1_1_11(__state, &__tmp3[0], &__tmp4, end, start);
        {
            double __inp = __tmp4;
            double __out;

            ///////////////////
            // Tasklet code (assign_34_8)
            __out = __inp;
            ///////////////////

            b[0] = __out;
        }
        delete[] __tmp3;

    }
    
}

void __program_spmv_internal(spmv_t *__state, double * __restrict__ A_data, unsigned int * __restrict__ A_indices, unsigned int * __restrict__ A_indptr, double * __restrict__ b, double * __restrict__ x, int M, int N, int nnz)
{

    {

        {
            #pragma omp parallel for
            for (auto __i0 = 0; __i0 < N; __i0 += 1) {
                {
                    double __out;

                    ///////////////////
                    // Tasklet code (assign_28_4)
                    __out = 0.0;
                    ///////////////////

                    b[__i0] = __out;
                }
            }
        }

    }
    {

        {
            #pragma omp parallel for
            for (auto i = 0; i < (M + 1); i += 1) {
                loop_body_0_1_0(__state, &A_data[0], &A_indices[0], &A_indptr[0], &x[0], &b[i], M, N, i, nnz);
            }
        }

    }
}

DACE_EXPORTED void __program_spmv(spmv_t *__state, double * __restrict__ A_data, unsigned int * __restrict__ A_indices, unsigned int * __restrict__ A_indptr, double * __restrict__ b, double * __restrict__ x, int M, int N, int nnz)
{
    __program_spmv_internal(__state, A_data, A_indices, A_indptr, b, x, M, N, nnz);
}

DACE_EXPORTED spmv_t *__dace_init_spmv(int M, int N, int nnz)
{
    int __result = 0;
    spmv_t *__state = new spmv_t;



    if (__result) {
        delete __state;
        return nullptr;
    }
    return __state;
}

DACE_EXPORTED void __dace_exit_spmv(spmv_t *__state)
{
    delete __state;
}

