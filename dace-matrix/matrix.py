import copy
import dace
from dace import dtypes
from dace.transformation.auto import auto_optimize
from IPython.display import Code


N, M, K = (dace.symbol(s, dtype=dace.int64) for s in ("N", "M", "K"))


@dace.program
def matrix_mul(A: dace.float64[N, K], B: dace.float64[K, M], C: dace.float64[N, M]):
    C[:] = A @ B


# Generate SDFG
sdfg = matrix_mul.to_sdfg()
cpu_sdfg = copy.deepcopy(sdfg)
gpu_sdfg = copy.deepcopy(sdfg)

# CPU code
cpu_sdfg = auto_optimize.auto_optimize(cpu_sdfg, device=dtypes.DeviceType.CPU)
code = Code(cpu_sdfg.generate_code()[0].clean_code, language="cpp")
with open("matrix.cpp", "w") as f:
    f.write(str(code))

# GPU code
gpu_sdfg = auto_optimize.auto_optimize(gpu_sdfg, device=dtypes.DeviceType.GPU)
code = Code(gpu_sdfg.generate_code()[0].clean_code, language="cuda")
with open("matrix.cu", "w") as f:
    f.write(str(code))
