/* DaCe AUTO-GENERATED FILE. DO NOT MODIFY */
#include <dace/dace.h>
#include "../../include/hash.h"
#include "mkl.h"
#include "../include/dace_blas.h"

struct matrix_mul_t {

};

void __program_matrix_mul_internal(matrix_mul_t *__state, double * __restrict__ A, double * __restrict__ B, double * __restrict__ C, long long K, long long M, long long N)
{

    {

        {
            double* _a = &A[0];
            double* _b = &B[0];
            double* _c = C;

            ///////////////////
            cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, M, N, K, double(1), _b, M, _a, K, double(0), _c, M);
            ///////////////////

        }

    }
}

DACE_EXPORTED void __program_matrix_mul(matrix_mul_t *__state, double * __restrict__ A, double * __restrict__ B, double * __restrict__ C, long long K, long long M, long long N)
{
    __program_matrix_mul_internal(__state, A, B, C, K, M, N);
}

DACE_EXPORTED matrix_mul_t *__dace_init_matrix_mul(long long K, long long M, long long N)
{
    int __result = 0;
    matrix_mul_t *__state = new matrix_mul_t;



    if (__result) {
        delete __state;
        return nullptr;
    }
    return __state;
}

DACE_EXPORTED void __dace_exit_matrix_mul(matrix_mul_t *__state)
{
    delete __state;
}

