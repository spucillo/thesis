/* DaCe AUTO-GENERATED FILE. DO NOT MODIFY */
#include <dace/dace.h>
#include "../../include/hash.h"
#include "../include/dace_cublas.h"

struct matrix_mul_t {
    dace::cuda::Context *gpu_context;
    double * __restrict__ __0_gpu_B;
    double * __restrict__ __0_gpu_A;
    double * __restrict__ __0_gpu_C;
    dace::blas::CublasHandle cublas_handle;
};

void __program_matrix_mul_internal(matrix_mul_t *__state, double * __restrict__ A, double * __restrict__ B, double * __restrict__ C, long long K, long long M, long long N)
{

    {

        cudaMemcpyAsync(__state->__0_gpu_A, A, (K * N) * sizeof(double), cudaMemcpyHostToDevice, __state->gpu_context->streams[0]);
        cudaMemcpyAsync(__state->__0_gpu_B, B, (K * M) * sizeof(double), cudaMemcpyHostToDevice, __state->gpu_context->streams[1]);

        cudaEventRecord(__state->gpu_context->events[0], __state->gpu_context->streams[1]);
        cudaStreamWaitEvent(__state->gpu_context->streams[0], __state->gpu_context->events[0], 0);

        {
            double * _a = &__state->__0_gpu_A[0];
            double * _b = &__state->__0_gpu_B[0];
            double* _c = __state->__0_gpu_C;

            ///////////////////
            int __dace_current_stream_id = 0;
            cudaStream_t __dace_current_stream = __state->gpu_context->streams[__dace_current_stream_id];
            const int __dace_cuda_device = 0;
            cublasHandle_t &__dace_cublas_handle = __state->cublas_handle.Get(__dace_cuda_device);
            cublasSetStream(__dace_cublas_handle, __dace_current_stream);
            cublasDgemm(__dace_cublas_handle,
            CUBLAS_OP_N, CUBLAS_OP_N,
            M, N, K,
            __state->cublas_handle.Constants(__dace_cuda_device).DoublePone(),
            (double*)_b, M,
            (double*)_a, K,
            __state->cublas_handle.Constants(__dace_cuda_device).DoubleZero(),
            (double*)_c, M);
            ///////////////////

        }
        cudaMemcpyAsync(C, __state->__0_gpu_C, (M * N) * sizeof(double), cudaMemcpyDeviceToHost, __state->gpu_context->streams[0]);
        cudaStreamSynchronize(__state->gpu_context->streams[0]);


    }
}

DACE_EXPORTED void __program_matrix_mul(matrix_mul_t *__state, double * __restrict__ A, double * __restrict__ B, double * __restrict__ C, long long K, long long M, long long N)
{
    __program_matrix_mul_internal(__state, A, B, C, K, M, N);
}
DACE_EXPORTED int __dace_init_cuda(matrix_mul_t *__state, long long K, long long M, long long N);
DACE_EXPORTED int __dace_exit_cuda(matrix_mul_t *__state);

DACE_EXPORTED matrix_mul_t *__dace_init_matrix_mul(long long K, long long M, long long N)
{
    int __result = 0;
    matrix_mul_t *__state = new matrix_mul_t;


    __result |= __dace_init_cuda(__state, K, M, N);
    cudaMalloc((void**)&__state->__0_gpu_B, (K * M) * sizeof(double));
    cudaMalloc((void**)&__state->__0_gpu_A, (K * N) * sizeof(double));
    cudaMalloc((void**)&__state->__0_gpu_C, (M * N) * sizeof(double));

    if (__result) {
        delete __state;
        return nullptr;
    }
    return __state;
}

DACE_EXPORTED void __dace_exit_matrix_mul(matrix_mul_t *__state)
{
    cudaFree(__state->__0_gpu_B);
    cudaFree(__state->__0_gpu_A);
    cudaFree(__state->__0_gpu_C);
    __dace_exit_cuda(__state);
    delete __state;
}

